#include <stdio.h>
#include "point.h"
/* An increment function that doesn't return a value. */
/*
 * struct Point {
 *   int x, y;
 *   void (*print_fptr) (const struct Point *p);
 *     };
 */

void Aweful(const double (*fptrArray[20]) (const int n, const double const *x));

int main() {
	struct Point pt;

	pt.x = 0;
	pt.y = 0;
    pt.print_fptr = NULL;
	printPoint(pt);

	movePoint_wrong(pt, 3);
	printf("After movePoint_wrong: ");
	printPoint(pt);

	movePoint (&pt, 3);
    pt.y += 1;
	printf("After movePoint: ");
    
    pt.print_fptr = &printPoint2;
    pt.print_fptr(&pt);

    pt.print_fptr = &printPoint3;
    pt.print_fptr(&pt);

	return 0;
}
