#include <stdio.h>
#include "point.h"

void movePoint_wrong(struct Point p, const int dist) {
  p.x += dist;
  p.y += dist;
}

void movePoint(struct Point *p, const int dist) {
  p->x += dist;
  p->y += dist;
}

void printPoint(struct Point p) {
  printf("x=%d, y=%d\n", p.x, p.y);
}

void printPoint2(const struct Point *p) {
  printf("\nx=%d, y=%d\n", p->x, p->y);
}

void printPoint3(const struct Point *p) {
  printf("\ny=%d, x=%d\n", p->y, p->x);
}

