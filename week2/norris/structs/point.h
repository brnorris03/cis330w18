struct Point {
  int x, y;
  void (*print_fptr) (const struct Point *p);
};

// Function prototypes
void movePoint_wrong(struct Point p, const int dist); 

void movePoint(struct Point *p, const int dist);

void printPoint(struct Point p);

void printPoint2(const struct Point *p);

void printPoint3(const struct Point *p);


