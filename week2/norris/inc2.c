#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void allocate(int **x, int n) {
	*x = (int *) malloc ( n * sizeof (int));
	memset(*x, 0, n * sizeof(int));
}

void increment (int *x, int n) {
	for (int i = 0; i < n; i++)
		x[i]++;
}

void deallocate(int **x) {
	free(*x);
}

int main() {
	int *y = NULL;
	allocate(&y, 3);
	
	increment(y, 3);
	
	for (int i = 0; i < 3; i++ ) {
		printf("%d\n", y[i]);
	}

	deallocate(&y);
	return 0;
}
