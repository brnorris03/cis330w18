#include "arr.h"

int main() {
	const int a=3, b =4, val=1;
	int **y;
	create(&y, a, b);
    y[3][4] = -1;
	init(y, a, b, val);
	print(y, a, b);
    del(&y, a, b);
	return 0;
}
