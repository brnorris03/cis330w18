#include <stdlib.h> // for malloc
#include <string.h> // for memset
#include <stdio.h>  // for printf

void allocate(int numRows, int numColumns, int ***pNumbers) {
  int i, j;
  (*pNumbers) = (int **) malloc ( numRows * sizeof(int *) );
  for (i = 0; i < numRows; i++) {
    (*pNumbers)[i] = (int *) malloc ( numColumns * sizeof(int) );
    for (j = 0; j < numColumns; j++) {
      (*pNumbers)[i][j] = -1;
    }
  }
}

// Another version of allocate
void allocate2(int numRows, int numColumns, int ***pNumbers) {
  int i, j;
  (*pNumbers) = (int **) malloc ( numRows * sizeof(int *) );
  int **numbers = *pNumbers; // alias so we don't have to keep dereferencing pNumbers
  for (i = 0; i < numRows; i++) {
    numbers[i] = (int *) malloc ( numColumns * sizeof(int) );
    memset(numbers[i], -1, numColumns * sizeof(int));  
  }
}

void print(const int nc, const int nr, const int **num) {
  for (int i = 0; i < nr; ++i) {
    for (int j = 0; j < nc; ++j) {
      printf("%d ", num[i][j]);
    }
    printf("\n");
  }

}

int main() {
  int **numbers;
  const int numRows = 3, numColumns = 3;
  int i, j;

  //allocate(numRows, numColumns, &numbers);
  allocate2(numRows, numColumns, &numbers);

  // do some work with numbers (ok to pass numbers because we don't change it)
  print(numRows, numColumns, (const int**) numbers);

  //All done, free it
  for (i = 0; i < numRows; ++i) 
    free(numbers[i]);
  free(numbers);
}
