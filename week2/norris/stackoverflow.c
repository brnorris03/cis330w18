#include <stdio.h>
typedef struct {
    double v[100];
} Chunky;

void foot(Chunky p) {
    printf("%f", p.v[0]); 
    foot(p);
    printf("%f", p.v[0]); 
}

int main() {
    Chunky t;
    t.v[0] = 1.2;
    foot(t);
}
