#include <iostream>

struct node
{
	int data;
};

node & makeNode(node &cref);

int main()
{
	node nodeA = {0};
	std::cout << "1: nodeA.data = " << nodeA.data << std::endl;
	makeNode(nodeA);
	std::cout << "2: nodeA.data = " << nodeA.data << std::endl;
	node nodeB;
	nodeB = makeNode(nodeA);
	std::cout << "3: nodeA.data = " << nodeA.data << std::endl; 
	std::cout << "1: nodeB.data = " << nodeB.data << std::endl;

	node nodeC;
	// makeNode(nodeB).data = 99;

	return 0;
}

node & makeNode(node &ref)
{
	std::cout << "call makeNode()\n";
	ref.data++;
	return ref;
}
