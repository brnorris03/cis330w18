// An example of subtype polymorphism
// http://www.catonmat.net/blog/cpp-polymorphism/
#include <iostream>
#include "cats.hpp"

/* Pass abstract base class pointer */
void meow(Felid *cat) {
 cat->meow();
}

int main() {
 Cat Fluffy;
 Tiger Tigger;
 Ocelot Silvia;

 //Fluffy.meow();
 //Tigger.meow();
 //Sam.meow();

 meow(&Fluffy);
 meow(&Tigger);
 meow(&Silvia);


}
