#ifndef __POINT_H
#define __POINT_H

struct PointStruct {
    int x;
    int y;
};
typedef struct PointStruct Point;

void setPoint(Point* , int , int );
void shoot(Point *);

#endif

