namespace funky {

class Shape {
    public:
        double computeArea();

    private:
        Shape(){}

};

class Square : public Shape {
    public:
        Square(double l) { length = l; }
        double computeArea();
    private:
        double length;
};
}
