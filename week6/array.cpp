#include <iostream>
class A {
public:
   A() : i(nullptr) {}
   A(const int num) { this->i = new int[num]; }
   ~A() { delete [] this->i; }
   int *i;
};

int main() {
   const int n = 10;
   int *arr = new int[n];
   for (int i = 0; i < n ; i++) {
      A arr2(3); 
      arr[i] = i;
      // Destructor for arr2 will be called
   } 

   delete [] arr;
   // std::cout << arr[2]; WRONG
   return 0;
}
