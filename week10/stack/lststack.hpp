/* Filename:  lststack.h

   Programmer:  Br. David Carlson

   Date:  March 28, 1998

   This is the header file to accompany LstStack.cpp.  They provide the
   LstStackClass shown below which is a subclass of StackBaseClass.
*/

#include "stack.hpp"
#include "list.hpp"


class LstStackClass: public StackBaseClass
   {
   public:
      // No constructor is mentioned here.  We instead use the default
      // constructor automatically supplied by the compiler.
      bool Empty(void) const;
      void Push(const ItemType & Item);
      void Pop(ItemType & Item);
   private:
      ListClass List;   // an embedded List object
   };


