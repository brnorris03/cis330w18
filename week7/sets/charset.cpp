#include <set>
#include <iostream>

using namespace std;

int main() {
    set<char> c; 
    c.insert('H');
    c.insert('e');
    c.insert('l');
    c.insert('p');

    for (auto it = c.begin(); it!=c.end(); it++) {
        cout << *it;
    }
    cout << endl;
}
