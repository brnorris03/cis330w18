#include <set>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
   set<int> s; 

   for(int i = 1; i <= 100; i++) { 
       s.insert(i); // Insert 100 elements, [1..100] 
   } 

   s.insert(42); // does nothing, 42 already exists in set 

   cout << s.size() << endl;

   /*
   cout << "Erasing ";
   int msize = s.size();
   for(int i = 2; i <= msize; i += 2) { 
      cout << i << " ";
      s.erase(i); // Erase even values 
   } 
   */
   for_each(s.begin(), s.end(),
           [&s](int i)->void { if (i % 2 == 0) { s.erase(i); cout << i << " ";} });
   cout << endl;

   int n = int(s.size()); // n will be 50 
   cout << "final size is " << n << endl;
}
