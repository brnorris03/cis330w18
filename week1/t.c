#include <stdio.h>   /* Needed for printf and getchar */

int main() 
{
    char userInput; 

    printf("Are you having a nice day? (y/n)? ");


    if (getchar() == 'y') {
        printf("That's wonderful, so am I!\n");
    } else {
        printf("That sucks, just try again tomorrow.\n");
    }

    return 0;
}
